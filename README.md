Declare-it
Этот документ описывает условия получения и использования информации мобильным приложением Declare it

Мы очень серьезно подходим к политике конфиденциальности.

Наше приложение "Declare it":

Не собирает никакой пользовательской информации без согласия пользователя.

Анонимная информация, которую мы получаем о пользователях от сторонних сервисов, таких как Google Analytics, Google Play и других включает в себя: информацию об установке (например, тип операционной системы и номер версии приложения), информацию об устройстве (аппаратная модель и версия операционной системы) и информацию о поведении пользователя.

Все данные, которые мы собираем используются только для внутреннего анализа и исследований в целях улучшения сервиса.

Мы не несем ответственность за последствия использования "Declare it", возникновения сбоев в устройствах или их поломки.

Наша политика конфиденциальности может меняться время от времени. Мы будем размещать какие-либо изменения Политики конфиденциальности на этой странице.

Если у Вас есть какие-то вопросы относительно нашей политики безопасности, пожалуйста, пишите:

neoname0q@gmail.com

Privacy Policy This Privacy Policy describes the information collected by mobile application Declare it.

We take user’s privacy seriously.

Declare it:

Do not collect any user information.

Anonymous information that we get about users from other services such as Google Analytics, Google Play, Yandex Metrika and others includes: information about the installation (for example, operating system type and application version number), information about the device (hardware model and the version of the operating system) and users behavior information.

All data that we collect is used for internal analysis and research to improve our products and make it better for you.

We are not responsible for the use of our applications of failures in devices or breakage.

Our Privacy Policy may change from time to time. We will post any Privacy Policy changes on this page.

If you have any questions or concerns about our privacy policies, please contact us: neoname0q@gmail.com